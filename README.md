# eps-apac-pc

## Scripts

[Extract elements from Content file](#extract-elements-from-content-file-extract_property_content.py)

[Create a content reference list from Content file](#create-a-content-reference-list-from-content-file-extract_content_reference.py)

---

## Extract elements from Content file ([extract_property_content.py](./scripts/extract_property_content.py))

Extract property level contents from Catalog/Content file in tsv (tab-separated values) format. 


#### Usage
1. Define ```keys``` to extract in the [file](./extract.py).
    
        keys = ['property_id','name','address/city','address/country_code']

2. Run the script with specifying a content file location.

        $ python3 extract.py PATH_TO_CONTENT_FILE

#### Output
    extract-{TIMESTAMP}.csv

#### Example
    property_id	name address/city	address/country_code
    33630029	Hostel Wizaya for backpacker	Yangon	    MM
    6899908	    Residence Le Palme	            Numana	    IT
    25814475	Ospitalità Insolita	            La Spezia	IT
    ...

---

## Create a content reference list from Content file ([extract_content_reference.py](./scripts/extract_content_reference.py))

Create a content reference list from Content file. This can be useful if you need the reference list in any languages.

#### Usage
Run the script with specifying a content file location.

    $ python3 extract_amenities.py PATH_TO_CONTENT_FILE

#### Output
    amenities-{LANGUAGE}.json

#### Example
[Content reference list in Korean](./samples/amenities-ko-KR.json)
