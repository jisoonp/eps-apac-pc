import sys
import json
import os

###
### extract_target = [ ('key in output file', 'key in content file', 'type')]
###     - type: 1   id, name
###             2   id, name, has_value                 
extract_target = [ ('categories', 'category', 1),
                   ('amenities_rooms', 'rooms/%/amenities', 2),
                   ('amenities_property', 'amenities', 2),
                   #('images', 'images'),
                   #('images', 'rooms/images'),
                   ('onsite_payment_types', 'onsite_payments/types', 1),
                   ('amenities_rates', 'rates/%/amenities', 2),
                   ('room_views', 'rooms/%/views', 1),
                   ('attributes_pets', 'attributes/pets', 2),
                   ('attributes_general', 'attributes/general', 2),
                   ('statistics', 'statistics', 2),
                   ('themes', 'themes', 1),
                   ('spoken_languages', 'spoken_languages', 1)
                ]

def remove_value(ref, dtype):
    if not type(ref) == dict:
        return ref
    if 'value' in ref:
        length = len(ref['value'])
        ref.pop('value')
        if 'name' in ref:
            ref['name'] = ref['name'][:-length-3].rstrip()
        ref['has_value'] = 'true'
    else:
        if dtype == 2:
            ref['has_value'] = 'false'
    return ref

def extract(data, keys, dtype):
    res = {}
    if len(keys) == 0:
        return data
    else:
        if keys[0] in data:
            values = extract(data[keys[0]], keys[1:], dtype)
            if not type(values) == list:
                for key in values.keys():
                    if not key in res:
                        res[key] = remove_value(values[key], dtype)
        elif keys[0] == '%':
            for key in data.keys():
                values = extract(data[key], keys[1:], dtype)
                for key in values.keys():
                    if not key in res:
                        res[key] = remove_value(values[key], dtype)
    return res

if __name__ == '__main__':
    dbfile = sys.argv[1]
    outfile = 'amenities-' + os.path.splitext(dbfile)[0][-5:] + '.json'
    out = open(outfile, 'w', encoding='utf-8')

    content_reference = {}
    for target in extract_target:
        content_reference[target[0]] = {}

    with open(dbfile, encoding='utf-8') as db:
        for line in db:
            property_data = json.loads(line, encoding='utf-8')
            for target in extract_target:
                data = content_reference[target[0]]
                try:
                    if target[0] == 'categories':
                        category = property_data[target[1]]
                        if not category['id'] in content_reference[target[0]]:
                            content_reference[target[0]][category['id']] = category
                    else:
                        keys = target[1].split('/')
                        values = extract(property_data, keys, target[2])

                        for key in values.keys():
                            if not key in data:
                                data[key] = values[key]
                except KeyError:
                    print(f"property_id: {property_data['property_id']}, target: {target[0]}")
    out.write(json.dumps(content_reference, sort_keys=True, indent=4, ensure_ascii=False))


