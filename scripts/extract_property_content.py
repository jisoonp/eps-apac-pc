import sys
import json
import os
from datetime import datetime

keys = ['property_id','name','address/city','address/country_code']

def get_value(json, key):
    elms = key.split('/')
    obj = json
    for elm in elms:
        if elm in obj:
            obj = obj[elm]
        else:
            return (False, None)
    return (True, obj)

dbfile = sys.argv[1]
outfile = 'extract-' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.csv'

out = open(outfile, 'w', encoding='utf-8')

out.write('\t'.join(keys) + '\n')
with open(dbfile) as db:
    for line in db:
        property_data = json.loads(line, encoding='utf-8')
        for key in keys:
            value = get_value(property_data, key)
            if value[0]:
                out.write('{}'.format(value[1].replace('\n','').replace('\r','').replace('\t','')))
            if key == keys[-1]:
                out.write('\n')
            else:
                out.write('\t')
