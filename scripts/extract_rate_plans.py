import sys
import json
import os
from datetime import datetime

keys = [ 
        ('#', ['property_id']),
        ('rooms', ['id', 'room_name']),
        ('rates', ['id', 'refundable', 'fenced_deal', 'amenities', 'occupancy_pricing/%/totals/inclusive/request_currency/value'])
       ]

def get_value(json, key):
    elms = key.split('/')
    obj = json
    for idx in range(len(elms)):
        elm = elms[idx]
        if elm in obj:
            obj = obj[elm]
        elif elm == '%':
            v = 0.0
            for k in obj.keys():
                res = get_value(obj[k], '/'.join(elms[idx+1:]))
                if res[0]:
                    v = v + float(res[1])
            return (True, v)
        else:
            return (False, None)
    return (True, obj)
    
def dfs(json, keys, extracted, out):
    if keys == []:
        for itm in extracted.keys():
            out.write(str(extracted[itm]) + '\t')
        out.write('\n')
        return

    child_key = keys[0][0]
    extract_keys = keys[0][1]

    child = []
    if child_key == '#':
        child = json
    else:
        child = json[child_key]

    for sub_child in child:
        for key in extract_keys:
            extracted[child_key + '/' + key] = get_value(sub_child, key)[1]
        dfs(sub_child, keys[1:], extracted, out)

avail_rs_file = sys.argv[1]

files = []
if os.path.isdir(avail_rs_file):
    files = [os.path.join(avail_rs_file, f) for f in os.listdir(avail_rs_file) if os.path.isfile(os.path.join(avail_rs_file, f))]
elif os.path.isfile(avail_rs_file):
    files = avail_rs_file
else:
    print(f'{sys.argv[1]} is not a file or a folder.')
    exit(1)

for infile in files:
    outfile = 'extract-rates-' + os.path.splitext(os.path.basename(infile))[0] + '.csv'
    out = open(outfile, 'w', encoding='utf-8')
    columns = [item for sublist in [x[1] for x in keys] for item in sublist] 
    out.write('\t'.join(columns) + '\n')
    with open(infile) as avail_rs:
        rs = json.load(avail_rs, encoding='utf-8')
        dfs(rs, keys, {}, out)
        